<?php
class Animal
{
    public $name;
    public $legs = 4;
    public $cold_blooded = "No";

    public function __construct($string) {
        $this->name = $string;
    }

    public function intro() {
        echo "Name: {$this->name} <br>";
        echo "Legs: {$this->legs} <br>";
        echo "Cold Blooded: {$this->cold_blooded} <br>"; 
        }
}

?>