<?php
require_once('animal.php');
class Frog extends Animal{
    public $cold_blooded = "Yes";

    public function jump()
    {
        echo "Hop Hop";
    }
}
?>