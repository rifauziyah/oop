<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("Shaun");
$sheep->intro();

echo "<br>";

$kodok = new Frog("Buduk");
$kodok->intro();
echo "Jump: ";
$kodok->jump();

echo "<br><br>";

$sungokong = new Ape("Kera Sakti");
$sungokong->intro();
echo "Yell: ";
$sungokong->yell();
?>